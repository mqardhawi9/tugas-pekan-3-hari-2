@extends('layout.master')

@section('judul')
Halaman Form Biodata
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
        <p>First name:</p>
        <input type="text" name="nmdepan">
        <p>Last name:</p>
        <input type="text" name="nmbelakang">
        <p>Gender:</p>
        <input type="radio" name="jenis_kelamin" value="Male">Male <br>
        <input type="radio" name="jenis_kelamin" value="Female">Female <br>
        <input type="radio" name="jenis_kelamin" value="Other">Other
        <p>Nationality:</p>
        <select name="nationality" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select>
        <p>Language Spoke:</p>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other 
        <p>Bio:</p>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="kirim">
    </form>
@endsection
    
