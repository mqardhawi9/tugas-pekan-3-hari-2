<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view('halaman.form');
    }

    public function Welcome(Request $request){
        $nmdepan = $request['nmdepan'];
        $nmbelakang = $request['nmbelakang'];
        return view('halaman.home', compact('nmdepan', 'nmbelakang'));
    }
}
